#  Internet Pour Les Piétons

Ce projet est destiné à produire un document LaTeX.

## Le document produit explique Internet aux non-informaticiens

Internet est déjà particulièrement important dans notre vie à tous.
Son importance semble continuer à augmenter.
Et pouratnt, je constate que c'est une boîte noire.
Tout le monde y va, l'utilise, en parle.
Mais presque personne ne comprend ce que c'est.
Les informations qui circulent à son sujet sont contradictoires.
Le non-initié ne peut pas savoir qui croire, quoi utiliser ou contre quoi se protéger.
Le but est d'aider le non-initié à comprendre ce qu'est Internet de façon à pouvoir comprendre les autres sources d'information.

## Lecture du document compilé
Le document compilé est disponible ici : [iplp.pdf](https://scarpet42.gitlab.io/iplp/iplp.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
